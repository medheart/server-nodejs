import {
  OK, NOT_FOUND, LAST_MODIFIED, NOT_MODIFIED, BAD_REQUEST, ETAG,
  CONFLICT, METHOD_NOT_ALLOWED, NO_CONTENT, CREATED, setIssueRes
} from './utils';
import Router from 'koa-router';
import {getLogger} from './utils';

const log = getLogger('heart');

let heartsLastUpdateMillis = null;

export class HeartRateRouter extends Router {
  constructor(props) {
    super(props);
    this.heartStore = props.heartStore;
    this.io = props.io;

    this.get('/', async(ctx) => {
      let res = ctx.response;
      let lastModified = ctx.request.get(LAST_MODIFIED);
      if (lastModified && heartsLastUpdateMillis && heartsLastUpdateMillis <= new Date(lastModified).getTime()) {
        log('search / - 304 Not Modified (the client can use the cached data)');
        res.status = NOT_MODIFIED;
      } else {
        res.body = await this.heartStore.find({});
        if (!heartsLastUpdateMillis) {
          heartsLastUpdateMillis = Date.now();
        }
        res.set({[LAST_MODIFIED]: new Date(heartsLastUpdateMillis)});
        log('search / - 200 Ok');
      }
    }).get('/:id', async(ctx) => {
      let heart = await this.heartStore.findOne({_id: ctx.params.id});
      let res = ctx.response;
      if (heart) {
        log('read /:id - 200 Ok');
        this.setHeartRes(res, OK, heart); //200 Ok
      } else {
        log('read /:id - 404 Not Found (if you know the resource was deleted, then you can return 410 Gone)');
        setIssueRes(res, NOT_FOUND, [{warning: 'Heart not found'}]);
      }
    }).post('/', async(ctx) => {
      let heart = ctx.request.body;
      log(JSON.stringify(heart));
      let res = ctx.response;
      if (heart.label || heart.bpm) { //validation
        heart.date = Date.now();
        await this.createHeart(res, heart);
      } else {
        log(`create / - 400 Bad Request`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Label or bpm is missing'}]);
      }
    }).put('/:id', async(ctx) => {
      let heart = ctx.request.body;
      let id = ctx.params.id;
      let heartId = heart._id;
      let res = ctx.response;
      if (heartId && heartId != id) {
        log(`update /:id - 400 Bad Request (param id and body _id should be the same)`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Param id and body _id should be the same'}]);
        return;
      }
      if (!heart.label) {
        log(`update /:id - 400 Bad Request (validation errors)`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Label is missing'}]);
        return;
      }
      if (!heart.bpm) {
        log(`update /:id - 400 Bad Request (validation errors)`);
        setIssueRes(res, BAD_REQUEST, [{error: 'Bpm is missing'}]);
        return;
      }
      if (!heartId) {
        await this.createHeart(res, heart);
      } else {
        let persistedHeart = await this.heartStore.findOne({_id: id});
        if (persistedHeart) {
          let heartVersion = parseInt(ctx.request.get(ETAG)) || heart.version;
   
          if (!heartVersion) {
            log(`update /:id - 400 Bad Request (no version specified)`);
            setIssueRes(res, BAD_REQUEST, [{error: 'No version specified'}]); //400 Bad Request
          } else if (heartVersion < persistedHeart.version) {
            log(`update /:id - 409 Conflict`);
            setIssueRes(res, CONFLICT, [{error: 'Version conflict'}]); //409 Conflict
          } else {
            heart.version = heartVersion + 1;
            heart.updated = Date.now();
            let updatedCount = await this.heartStore.update({_id: id}, heart);
            heartsLastUpdateMillis = heart.updated;
            if (updatedCount == 1) {
              this.setHeartRes(res, OK, heart); //200 Ok
              this.io.emit('heart-updated', heart);
            } else {
              log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
              setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'Heart no longer exists'}]); //
            }
          }
        } else {
          log(`update /:id - 405 Method Not Allowed (resource no longer exists)`);
          setIssueRes(res, METHOD_NOT_ALLOWED, [{error: 'Heart no longer exists'}]); //Method Not Allowed
        }
      }
    }).del('/:id', async(ctx) => {
      let id = ctx.params.id;
      await this.heartStore.remove({_id: id});
      this.io.emit('heart-deleted', {_id: id})
      heartsLastUpdateMillis = Date.now();
      ctx.response.status = NO_CONTENT;
      log(`remove /:id - 204 No content (even if the resource was already deleted), or 200 Ok`);
    });
  }

  async createHeart(res, item) {
    item.version = 1;
    item.updated = Date.now();
    let insertedItem = await this.heartStore.insert(item);
    heartsLastUpdateMillis = item.updated;
    this.setHeartRes(res, CREATED, insertedItem); //201 Created
    this.io.emit('heart-created', insertedItem);
  }

  setHeartRes(res, status, item) {
    res.body = item;
    res.set({
      [ETAG]: item.version,
      [LAST_MODIFIED]: new Date(item.updated)
    });
    res.status = status; //200 Ok or 201 Created
  }
}
