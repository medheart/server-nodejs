import Koa from 'koa';
import cors from 'koa-cors';
import convert from 'koa-convert';
import bodyParser from 'koa-bodyparser';
import Router from 'koa-router';
import http from 'http';
import socketIo from 'socket.io';
import dataStore from 'nedb-promise';
import {getLogger, timingLogger, errorHandler} from './utils';
import {AuthRouter, jwtConfig} from './auth-router';
import {HeartRateRouter} from './heart-rate-router'
import koaJwt from 'koa-jwt';

const app = new Koa();
const router = new Router();
const server = http.createServer(app.callback());
const io = socketIo(server);
const log = getLogger('app');

import {API_URL, HEART_RATE_ROUTE, AUTH_ROUTE} from './constants.js';

app.use(timingLogger);
app.use(errorHandler);

app.use(bodyParser());
app.use(convert(cors()));


log('config public routes');
const authApi = new Router({prefix: API_URL});
const userStore = dataStore({filename: '../users.json', autoload: true});

authApi.use(AUTH_ROUTE,new AuthRouter({userStore,io}).routes());
app.use(authApi.routes()).use(authApi.allowedMethods());

log("config private routes");
app.use(convert(koaJwt(jwtConfig)));

const protectedApi = new Router({prefix: API_URL})
const heartStore = dataStore({filename: '../heartData.json', autoload: true});

protectedApi.use(HEART_RATE_ROUTE, new HeartRateRouter({heartStore, io}).routes())
app.use(protectedApi.routes()).use(protectedApi.allowedMethods());

log('config socket io');
io.on('connection', (socket) => {
    log('client connected');
    socket.on('disconnect', () => {
        log('client disconnected');
    })
});


log("add data");
(async() => {
    log('ensure default data');
    let admin = await userStore.findOne({username: 'admin'});
    if (admin) {
        log(`admin user was in the store`)
    } else {
        admin = await userStore.insert({username: 'admin', password: 'admin'});
        log(`admin added ${JSON.stringify(admin)}`);
    }

    let hearts= await heartStore.find({});
    if (hearts.length > 0) {
        log(`heartData store has ${hearts.length} heart data`)
    } else {
        log(`heartRate store was empty, adding some heartData`)
        for (let i = 0; i < 3; i++) {
            let heart = await heartStore.insert({label: `heartData ${i}`, bpm: "23",date:Date.now(),version:1, updated: Date.now(), user: admin._id});
            log(`heartData added ${JSON.stringify(heart)}`);
        }
    }
})();

log("end data init");

app.listen(3000);
